<?php

declare(strict_types=1);

use Zend\ConfigAggregator\ArrayProvider;
use Zend\ConfigAggregator\ConfigAggregator;
use Zend\ConfigAggregator\PhpFileProvider;

// To enable or disable caching, set the `ConfigAggregator::ENABLE_CACHE` boolean in
// `config/autoload/local.php`.
$cacheConfig = [
    // Config caching is tricky and poorly documented...
    // ConfigAggregator::ENABLE_CACHE => true, - this only controls whether the aggregated config should be written to a file.
    // When `composer create-project zendframework/zend-expressive-skeleton <path>` is used to install the
    // zend expressive skeleton application it should run `composer development-enable` as a post-install script.
    // Sometimes (part of the) process fails and the config/development.config.php and the config/autoload/development.local.php
    // files are not being created properly. Then, on the first run, the aggregated config WILL be cached to the specified file path.
    // From then on the cached config will be used until purged. To fix this:
    // 1. Run `composer development-enable` manually to create the necessary development config files.
    // 2. Delete the data/cache/config-cache.php file. (or run `composer clear-config-cache`)
    // Or to disable config cache from being stored and read altogether pass `'config_cache_path' => null,`
    // See:
    // README.md:114
    // vendor/zendframework/zend-config-aggregator/src/ConfigAggregator.php:64
    // vendor/zendframework/zend-config-aggregator/src/ConfigAggregator.php:239
    // vendor/zendframework/zend-config-aggregator/src/ConfigAggregator.php:257
    'config_cache_path' => null,
    //'config_cache_path' => 'data/cache/config-cache.php', // This is the initial value when the project is created
];

$aggregator = new ConfigAggregator([
    \Smtm\Zfx\Db\ConfigProvider::class,
    \Smtm\ZeDoctrine\ConfigProvider::class,
    \Smtm\Crawlbot\ConfigProvider::class,
    \Zend\Db\ConfigProvider::class,
    \Zend\Form\ConfigProvider::class,
    \Zend\Hydrator\ConfigProvider::class,
    \Zend\InputFilter\ConfigProvider::class,
    \Zend\Filter\ConfigProvider::class,
    \Zend\Mvc\I18n\ConfigProvider::class,
    \Zend\I18n\ConfigProvider::class,
    \Zend\HttpHandlerRunner\ConfigProvider::class,
    \Zend\Expressive\ZendView\ConfigProvider::class,
    \Zend\Expressive\Router\ZendRouter\ConfigProvider::class,
    \Zend\Router\ConfigProvider::class,
    \Zend\Validator\ConfigProvider::class,
    // Include cache configuration
    new ArrayProvider($cacheConfig),

    \Zend\Expressive\Helper\ConfigProvider::class,
    \Zend\Expressive\ConfigProvider::class,
    \Zend\Expressive\Router\ConfigProvider::class,

    // Swoole config to overwrite some services (if installed)
    class_exists(\Zend\Expressive\Swoole\ConfigProvider::class)
        ? \Zend\Expressive\Swoole\ConfigProvider::class
        : function(){ return[]; },

    // Default App module config
    App\ConfigProvider::class,

    // Load application config in a pre-defined order in such a way that local settings
    // overwrite global settings. (Loaded as first to last):
    //   - `global.php`
    //   - `*.global.php`
    //   - `local.php`
    //   - `*.local.php`
    new PhpFileProvider(realpath(__DIR__) . '/autoload/{{,*.}global,{,*.}local}.php'),

    // Load development config if it exists
    new PhpFileProvider(realpath(__DIR__) . '/development.config.php'),
], $cacheConfig['config_cache_path']);

return $aggregator->getMergedConfig();
