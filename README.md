## Poorly documented features
### Configuration caching
ConfigAggregator::ENABLE_CACHE => true, - this only controls whether the aggregated config should be written to a file.    
When `composer create-project zendframework/zend-expressive-skeleton <path>` is used to install the    
zend expressive skeleton application it should run `composer development-enable` as a post-install script.    
Sometimes (part of the) process fails and the config/development.config.php and the config/autoload/development.local.php    
files are not being created properly. Then, on the first run, the aggregated config WILL be cached to the specified file path.    
From then on the cached config will be used until purged. To fix this:    
1. Run `composer development-enable` manually to create the necessary development config files.    
2. Delete the data/cache/config-cache.php file. (or run `composer clear-config-cache`)    
Or to disable config cache from being stored and read altogether pass `'config_cache_path' => null,`    
See:    
README.md:114    
vendor/zendframework/zend-config-aggregator/src/ConfigAggregator.php:64    
vendor/zendframework/zend-config-aggregator/src/ConfigAggregator.php:239    
vendor/zendframework/zend-config-aggregator/src/ConfigAggregator.php:257    

### Enabling routes and pipeline auto-wiring
[How to](https://github.com/zendframework/zend-expressive/blob/master/docs/book/v3/cookbook/autowiring-routes-and-pipelines.md)    
[How NOT to](https://framework.zend.com/blog/2017-03-30-expressive-config-routes.html)    
...    
"    
In order to enable the delegator factory, you will need to define the following service configuration somewhere, either at the application level in a config/autoload/ file, or within a module-specific ConfigProvider class:
```
return [
    'dependencies' => [
        'delegators' => [
            \Zend\Expressive\Application::class => [
                \Zend\Expressive\Container\ApplicationConfigInjectionDelegator::class,
            ],
        ],
    ],
];
```
"    
